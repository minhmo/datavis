const { MDCSelect } = mdc.select;

var margin = { top: 50, right: window.innerWidth / 2 + 50, bottom: 20, left: 50 };
const width = window.innerWidth - margin.left - margin.right;
const height = window.innerHeight / 2 - margin.top - margin.bottom;

let data = [];
let sub1Name, sub2Name, subName, sub1Value, subs, dataSet;

let countriesDisplayed = [];

const numFormat = d3.format(".2s");

let svg, wraper, controlsElement;

var x = d3
  .scaleBand()
  .range([0, width])
  .padding(0.2);

var y = d3
  .scaleLinear()
  .range([height, 0])
  .nice();

var color = d3.scaleOrdinal().range(["#98abc5", "#8a89a6"]);

var xAxis = d3.axisBottom().scale(x);

var yAxis = d3
  .axisLeft()
  .scale(y)
  .tickFormat(numFormat);

var yValue = "International tourism, number of arrivals and departures";
var xValue = "2010";
var sOrder = "name-ascending";

var key = ["sub1Value", "sub2Value"];

var transition = d3
  .transition()
  .duration(750)
  .ease(d3.easeCubic);

function draw(countries, year, unfilteredData, rootElement, controls, mapHeight) {
  svg = rootElement;
  data = unfilteredData;
  controlsElement = controls;
  countriesDisplayed = countries;
  xValue = year;

  svg.attr("transform", `translate(0, ${mapHeight + 150})`);

  loadControls(controlsElement.attr("id"), "../views/histogram.html");
  controlsElement.attr("y", "240");

  drawHistogram(year, yValue, sOrder, countries);
}

function drawHistogram(
  xValue,
  yValue,
  sOrder,
  countries = ["China", "United Kingdom", "Spain", "Germany", "Australia"]
) {
  var xValue = xValue;
  var yValue = yValue;
  var countries = countries;
  var sOrder = sOrder;

  svg.selectAll("*").remove();

  wraper = svg.append("g");

  // Part 1: Data Preprocessing
    // 0) Get the two sub items of the selected item
  subs = Total_to_Sub(yValue);
  sub1Name = subs.sub1Name;
  sub2Name = subs.sub2Name;
    // 1) Filter the data by the sub items
  dataSet = data.filter(function(d) {
    return d.Indicator == sub1Name || d.Indicator == sub2Name;
  });
    // 2) Merge the data by the country name
  let dataSet_Merge = Merge_sub_dataset(dataSet);
    // 3) Replace null values by 0; and generate new data structure
  let dataSet_remove_NaN = remove_NaN(
    dataSet_Merge,
    sub1Name,
    sub2Name,
    xValue
  );
    // 4) Select the data by the selected countries from the world map
  let dataPlay = dataSet_remove_NaN.filter(function(d) {
    var res = false;
    for (var i = 0; i < countries.length; i++) {
      if (d["Country Name"] == countries[i]) {
        res = true;
        break;
      }
    }
    return res;
  });
    // 5) sort the data a) ascending b) descending c) alphabetical
  dataPlay = sort_Data(dataPlay, sOrder);

  x.domain(dataPlay.map(function(d) {return d["Country Name"];}));
  y.domain([0,d3.max(dataPlay, function(d) {return d.sub1Value + d.sub2Value;})]);
  color.domain([sub1Name, sub2Name]);

  const axisMarginLeft = 50;

  // Part 2: Draw the histogram
  key = ["sub1Value", "sub2Value"];
  let nameKeys = dataPlay.map(function(d) {
    return d["Country Name"];
  });
  let legendNames = [LegendName(sub1Name), LegendName(sub2Name)];
  const stack = d3.stack().keys(key);

    //add data for missing bars
  const seriesFlipped = stack(
    dataPlay.map(d => {
      const defaultData = {};
      key.forEach(k => (defaultData[k] = 0));
      return Object.assign(defaultData, {
        sub1Value: d.sub1Value,
        sub2Value: d.sub2Value
      });
    })
  );

  const series = [];
  //We need to reorient the series
  //we want a list of groups, not a list of rects from each level
  seriesFlipped[0].forEach((col, i) => {
    const arr = [];
    seriesFlipped.forEach((row, index2) => {
      //mimic the key from the grouped data format
      row[i].key = index2 + 1 + "";
      arr.push(row[i]);
    });
    series.push(arr);
  });

  const barSections = wraper
    .append("g")
    .selectAll("g")
    .data(series);

  const bars = barSections
    .enter()
    .append("g")
    .merge(barSections)
    .attr("transform", (d, i) => {
      return "translate(" + x(nameKeys[i]) + ",0)";
    })
    .selectAll("rect")
    .data(d => d, d => d.key);

  const enterBars = bars
    .enter()
    .append("rect")
    .attr("fill", function(d) {
      return color(d.key);
    })
    .attr("class", "histogram-bar");

  bars
    .exit()
    .transition()
    .style("opacity", 0)
    .remove();

  enterBars
    .merge(bars)
    .attr("x", margin.left)
    .attr("width", x.bandwidth())
    .attr("y", height - margin.top - 30)
    .attr("height", 0)
    .transition(transition)
    .attr("y", function(d) {
      return y(d[1]);
    })
    .attr("height", function(d) {
      return y(d[0]) - y(d[1]);
    });

  enterBars
    .on("mouseover", function() {
      tooltip.style("visibility", "visible");
    })
    .on("mousemove", function(d) {
      var xPosition = d3.event.pageX - 45;
      var yPosition = d3.event.pageY - 330;
      tooltip
        .style("left", d3.event.pageX + 25 + "px")
        .style("top", d3.event.pageY + "px")
        .select("p")
        .text(tooltipTexts(d, yValue));
    })
    .on("mouseout", function() {
      tooltip.style("visibility", "hidden");
    });
    // Draw x Axis
  const gx = wraper
    .append("g")
    .attr("class", "axis")
    .attr("id", "xAxis")
    .attr(
      "transform",
      `translate(${axisMarginLeft}, ${height})`
    )
    .call(xAxis)
    .selectAll("text")
    .style("font-family", "")
    .attr("class", "mdc-typography--caption")
    .style("text-anchor", "end")
    .attr("transform", function(d) {
      return "rotate(-65)";
    });
    // Draw y Axis
  const gy = wraper
    .append("g")
    .attr("class", "axis")
    .attr("id", "yAxis")
    .attr("transform", `translate(${axisMarginLeft}, 0)`)
    .call(yAxis)
    .selectAll("text")
    .style("font-family", "")
    .attr("class", "mdc-typography--caption");

  //Part 3: Add legend
  var legend = svg
    .append("g")
    .attr("transform", "translate(200, 0)")
    .attr("text-anchor", "end");

  legend
    .append("text")
    .text("Sub Items")
    .attr("x", width - 150)
    .attr("class", "mdc-typography--caption");

  var legendEnter = legend
    .selectAll("g")
    .data(legendNames)
    .enter()
    .append("g")
    .attr("transform", function(d, i) {
      return "translate(0," + i * 20 + ")";
    });

  legendEnter
    .append("rect")
    .attr("x", width - 170)
    .attr("y", 10)
    .attr("width", 19)
    .attr("height", 19)
    .attr("fill", color);

  legendEnter
    .append("text")
    .attr("x", width - 175)
    .attr("y", 20)
    .attr("dy", "0.32em")
    .style("font-size", "8px")
    .attr("class", "mdc-typography--caption")
    .text(d => d);
  
    //Part 4: Add tooltip
  var tooltip = d3
    .select("body")
    .append("div")
    .style("visibility", "hidden")
    .attr("class", "tooltip mdc-card mdc-theme--on-surface");

  tooltip.append("p");
}

function loadControls(element, path) {
  var xhr = new XMLHttpRequest();
  xhr.open("GET", path, true);
  xhr.onreadystatechange = function() {
    if (this.readyState !== 4) return;
    if (this.status !== 200) return; // or whatever error handling you want
    document.getElementById(element).innerHTML = this.responseText;

    let item = new MDCSelect(document.querySelector("#xAxisItem"));
    item.listen("MDCSelect:change", () => {
      selectVariable(0);
    });

    item = new MDCSelect(document.querySelector("#yAxisItem"));
    item.listen("MDCSelect:change", () => {
      selectVariable(1);
    });

    item = new MDCSelect(document.querySelector("#sort"));
    item.listen("MDCSelect:change", () => {
      selectVariable(2);
    });
  };
  xhr.send();
}

// Onchange function for select object
function selectVariable(id) {
  if (id == 0) {
    var e = document.getElementById("xAxisItemSelect");
    xValue = e.options[e.selectedIndex].value;
  } else if (id == 1) {
    var e = document.getElementById("yAxisItemSelect");
    yValue = e.options[e.selectedIndex].value;
  } else {
    var e = document.getElementById("sortSelect");
    sOrder = e.options[e.selectedIndex].value;
  }
  drawHistogram(xValue, yValue, sOrder, countriesDisplayed);

  return svg;
}

// Get the sub_item names given the total item name
function Total_to_Sub(totalName) {
  if (totalName === "International tourism, expenditures (current US$)") {
    sub1Name =
      "International tourism, expenditures for passenger transport items (current US$)";
    sub2Name =
      "International tourism, expenditures for travel items (current US$)";
  } else if (totalName === "International tourism, receipts (current US$)") {
    sub1Name =
      "International tourism, receipts for passenger transport items (current US$)";
    sub2Name = "International tourism, receipts for travel items (current US$)";
  } else {
    sub1Name = "International tourism, number of arrivals";
    sub2Name = "International tourism, number of departures";
  }
  return { sub1Name, sub2Name };
}

// Get the short name for legend
function LegendName(name) {
  if (
    name ==
      "International tourism, expenditures for passenger transport items (current US$)" ||
    name ==
      "International tourism, receipts for passenger transport items (current US$)"
  ) {
    return "Transport";
  } else if (
    name ==
      "International tourism, expenditures for travel items (current US$)" ||
    name == "International tourism, receipts for travel items (current US$)"
  ) {
    return "Travel";
  } else if (name == "International tourism, number of arrivals") {
    return "Arrivals";
  } else if (name == "International tourism, number of departures") {
    return "Departures";
  }
}

function tooltipTexts(d, y_label) {
  var unit;
  if (y_label == "International tourism, number of arrivals and departures") {
    unit = "";
  } else {
    unit = "$";
  }
  if (d[0] == 0) {
    return LegendName(sub1Name) + ": " + (d[1] - d[0]) + unit;
  } else {
    return LegendName(sub2Name) + ":" + (d[1] - d[0]) + unit;
  }
}

// Merge sub dataset
function Merge_sub_dataset(data) {
  data.sort(function(a, b) {
    var x = a["Country Name"].toLowerCase();
    var y = b["Country Name"].toLowerCase();
    if (x < y) {
      return -1;
    }
    if (x > y) {
      return 1;
    }
    return 0;
  });
  return data;
}

// Remove NaN value, and generate new dataset {Country Nanme, Sub1Value, Sub2Value} , which has Country Name as the key
function remove_NaN(data, sub1Name, sub2Name, year) {
  var i;
  var value;
  var countries = [];
  var countryName = data[0]["Country Name"];
  var count = 0; // The number of unique countries
  var flag = 0; // Judge whether both sub1Name and sub2Name exist
  for (i = 0; i < data.length; i++) {
    subName = data[i]["Indicator"];
    // First, replace NaN with 0, others not change
    value = parseInt(data[i][year]) || 0;
    // Second, Create the object: {Country Nanme, Sub1Value, Sub2Value} for the chosen year
       // If there is only one indicator exists (either Sub1Value or Sub2Value), then the missing one will be filled with 0
    if (flag == 0) {
      if (subName == sub1Name) {
        flag = 1;
        countries.push({
          "Country Name": data[i]["Country Name"],
          sub1Value: value
        });
      } else {
        countries.push({
          "Country Name": data[i]["Country Name"],
          sub1Value: 0,
          sub2Value: value
        });
        count = count + 1;
      }
    } else if (data[i]["Country Name"] === countryName && flag === 1) {
      // Adjust the current country: set sub2Value to value
      sub1Value = countries[count].sub1Value;
      countries[count] = {
        "Country Name": countryName,
        sub1Value: sub1Value,
        sub2Value: value
      };
      count = count + 1;
      flag = 0;
    } else {
      //data[i] !== countryName && flag === 1
      // Adjust the current country: its sub2Name is missing, set sub2Value to 0
      sub1Value = countries[count].sub1Value;
      countries[count] = {
        "Country Name": countryName,
        sub1Value: sub1Value,
        sub2Value: 0
      };
      count = count + 1;
      // Add the new country
      if (subName === sub1Name) {
        countries.push({
          "Country Name": data[i]["Country Name"],
          sub1Value: value
        });
      } else {
        countries.push({
          "Country Name": data[i]["Country Name"],
          sub1Value: 0,
          sub2Value: value
        });
        count = count + 1;
        flag = 0;
      }
    }
    countryName = data[i]["Country Name"];
  }
  return countries;
}

function sort_Data(data, sOrder) {
  if (sOrder == "name-ascending") {
    return data.sort(function(a, b) {
      return a["Country Name"].toLowerCase() - b["Country Name"].toLowerCase();
    });
  } else if (sOrder == "value-ascending") {
    return data.sort(function(a, b) {
      return a.sub1Value + a.sub2Value - b.sub1Value - b.sub2Value;
    });
  } else {
    return data.sort(function(a, b) {
      return -a.sub1Value - a.sub2Value + b.sub1Value + b.sub2Value;
    });
  }
}

function destroy() {
  svg.selectAll("*").remove();
  controlsElement.selectAll("*").remove();
}

const Histogram = {
  draw: draw,
  destroy: destroy,
  properties: {
    height: height,
    width: width,
    margin: margin
  }
};

export { Histogram };