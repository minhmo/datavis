const { MDCSelect } = mdc.select;

var viewWidth = window.innerWidth;
var viewHeight = window.innerHeight / 2;

var margin = {
  top: 50,
  right: 100,
  bottom: 20,
  left: window.innerWidth / 2
};
var width = viewWidth - margin.left - margin.right;
var height = viewHeight - margin.top - margin.bottom;

var x = d3.scaleLinear().range([0, width]);
var y = d3.scaleLinear().range([height, 0]);

const numFormat = d3.format(".2s");
var xAxis = d3
  .axisBottom()
  .scale(x)
  .tickFormat(numFormat);
var yAxis = d3
  .axisLeft()
  .scale(y)
  .tickFormat(numFormat);

let svg;

// The variables of xAxis and yAxis
const variables = [
  "International tourism, number of arrivals",
  "International tourism, number of departures",
  "International tourism, expenditures (current US$)",
  "International tourism, receipts (current US$)",
  "Commercial service exports (current US$)",
  "Commercial service imports (current US$)",
  "GDP (current US$)",
  "Total reserves (includes gold, current US$)"
];

// Initial parameters for xAxis variable, yAxis variable, countries, and year
var xValue = "International tourism, number of arrivals";
var yValue = "International tourism, expenditures (current US$)";
var countriesSelected = [
  "China",
  "United Kingdom",
  "Spain",
  "Germany",
  "Australia",
  "United States",
  "Germany",
  "France",
  "Japan",
  "Italy"
];


var points = null;
var data = [];
let texts = [];

let controlsElement = {};

class Scatterplot {
  constructor(mapHeight, data, rootElement, controls) {
    svg = rootElement;
    controlsElement = controls;

    svg.attr(
      "transform",
      `translate(${viewWidth / 2 + 50}, ${mapHeight + 150})`
    );

    loadControls(controlsElement.attr("id"), "../views/scatterplot.html");
    controlsElement.attr("y", "240");
    controlsElement.attr("x", viewWidth / 2 + 50);

    // data Preprocessing
    // 1. Filter by the selected countries from the world map.
    data = data.filter(function(d) {
      var res = false;
      for (var i = 0; i < countriesSelected.length; i++) {
        if (d["Country Name"] == countriesSelected[i]) {
          res = true;
          break;
        }
      }
      return res;
    });

    // 2. Keep all potential variables and remove all the others
    data = data.filter(function(d) {
      return (
        d.Indicator == variables[0] ||
        d.Indicator == variables[1] ||
        d.Indicator == variables[2] ||
        d.Indicator == variables[3] ||
        d.Indicator == variables[4] ||
        d.Indicator == variables[5] ||
        d.Indicator == variables[6] ||
        d.Indicator == variables[7] 
      );
    });

    // 3. Merge the data by country
    data = Sort_by_country(data);
    data = Merge_by_country(data, "2010");
  }

  draw(year, unfiltereddata, countries) {
    countriesSelected = countries;

    // data Preprocessing
    // 1. Filter by the selected countries from the world map.
    data = unfiltereddata.filter(function(d) {
      var res = false;
      for (var i = 0; i < countriesSelected.length; i++) {
        if (d["Country Name"] == countriesSelected[i]) {
          res = true;
          break;
        }
      }
      return res;
    });

    // 2. Keep all potential variables and remove all the others
    data = data.filter(function(d) {
      return (
        d.Indicator == variables[0] ||
        d.Indicator == variables[1] ||
        d.Indicator == variables[2] ||
        d.Indicator == variables[3] ||
        d.Indicator == variables[4] ||
        d.Indicator == variables[5] ||
        d.Indicator == variables[6] ||
        d.Indicator == variables[7] 
      );
    });


    // 3. Merge the data by country
    data = Sort_by_country(data);
    data = Merge_by_country(data, year);

    drawScatterplot(xValue, yValue, data);
    controlsElement.style("visibility", "visible");
  }

  destroy() {
    svg.selectAll("*").remove();
    controlsElement.style("visibility", "hidden");
  }
}

// Draw Scatterplot
function drawScatterplot(xValue, yValue, data) {
  var xExtent = d3.extent(data, function(d) {
    return d[dataName(xValue)];
  });
  var yExtent = d3.extent(data, function(d) {
    return d[dataName(yValue)];
  });

  x.domain(xExtent).nice();
  y.domain(yExtent).nice();

  svg.selectAll("g").remove();

  // Draw x axis
  svg
    .append("g")
    .attr("id", "xAxis_scatt")
    .attr("class", "axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis)
    .append("text")
    .attr("class", "label")
    .attr("id", "xLabel")
    .attr("x", width)
    .attr("y", -6)
    .style("text-anchor", "end")
    .style("fill", "black")
    .text(xValue);
  // Draw y axis
  svg
    .append("g")
    .attr("id", "yAxis_scatt")
    .attr("class", "axis")
    .call(yAxis)
    .append("text")
    .attr("class", "label")
    .attr("id", "yLabel")
    .attr("transform", "rotate(-90)")
    .attr("y", 6)
    .attr("dy", ".71em")
    .style("text-anchor", "end")
    .style("fill", "black")
    .text(yValue);
  // Draw the points in the scatterplot
  points = svg
    .append("g")
    .attr("class", "plotArea")
    .selectAll(".dot")
    .data(data)
    .enter()
    .append("circle")
    .attr("class", "dot")
    .attr("r", 10)
    .attr("cx", function(d) {
      const value = d[dataName(xValue)];
      return x(value);
    })
    .attr("cy", function(d) {
      const value = d[dataName(yValue)];
      return y(value);
    })
    .style("opacity", "0.5")
    .style("fill", "red");
  // Add the country name per point
  texts = svg
    .append("g")
    .selectAll(".text")
    .data(data)
    .enter()
    .append("text")
    .attr("class", "mdc-typography--caption")
    .attr("x", function(d) {
      return x(d[dataName(xValue)]) - 15;
    })
    .attr("y", function(d) {
      return y(d[dataName(yValue)]) + 20;
    })
    .text(function(d) {
      return d["country"];
    });
}

// Update the scatterplot
function updatePoints(v1, v2) {
  if (points == null) {
    return;
  }
  var xExtent = d3.extent(data, function(d) {
    return d[dataName(v1)];
  });
  var yExtent = d3.extent(data, function(d) {
    return d[dataName(v2)];
  });

  x.domain(xExtent).nice();
  y.domain(yExtent).nice();

  xAxis.scale(x);
  yAxis.scale(y);

  d3.select("#xLabel").text(v1);
  d3.select("#yLabel").text(v2);

  d3.select("#xAxis_scatt").call(xAxis);
  d3.select("#yAxis_scatt").call(yAxis);


  var transition = d3
    .transition()
    .duration(750)
    .ease(d3.easeCubic);

  points
    .transition(transition)
    .attr("cx", function(d) {
      return x(d[dataName(v1)]);
    })
    .attr("cy", function(d) {
      return y(d[dataName(v2)]);
    })
    .style("fill", "red");

  texts
    .transition(transition)
    .attr("x", function(d) {
      return x(d[dataName(v1)]) - 20;
    })
    .attr("y", function(d) {
      return y(d[dataName(v2)]) + 20;
    });
}

function selectVariable(id) {
  if (id == 0) {
    var e = document.getElementById("xAxisItem_scat");
    xValue = e.options[e.selectedIndex].value;
  } else if (id == 1) {
    var e = document.getElementById("yAxisItem_scat");
    yValue = e.options[e.selectedIndex].value;
  }
  updatePoints(xValue, yValue);
}

function Sort_by_country(data) {
  data.sort(function(a, b) {
    var x = a["Country Name"].toLowerCase();
    var y = b["Country Name"].toLowerCase();
    if (x < y) {
      return -1;
    }
    if (x > y) {
      return 1;
    }
    return 0;
  });
  return data;
}

function Merge_by_country(data, year) {
  var i;
  var j = 0;
  var value;
  var res = [];
  var Countries = countriesSelected.sort();

  //Initilize the new data structure with only the variables needed
  for (i = 0; i < Countries.length; i++) {
    res.push({
      country: countriesSelected[i],
      arrival: null,
      departure: null,
      expenditure: null,
      receipt: null,
      serviceImport: null,
      serviceExport: null,
      GDP: null,
      reserve: null
    });
  }

  // Feed the new data sturcture with the values from the original data set
  for (i = 0; i < data.length; i++) {
    value = parseInt(data[i][year]) || 0;
    
    if (data[i]["Country Name"] != Countries[j]) {
      j = j + 1;
    }

    if (data[i]["Indicator"] == "International tourism, number of arrivals"){
      res[j].arrival = value;
    } else if (data[i]["Indicator"] == "International tourism, number of departures") {
      res[j].departure = value;
    } else if (data[i]["Indicator"] == "International tourism, expenditures (current US$)"){
      res[j].expenditure = value;
    } else  if (data[i]["Indicator"] == "International tourism, receipts (current US$)"){
      res[j].receipt = value;
    } else  if (data[i]["Indicator"] == "Commercial service imports (current US$)"){
      res[j].serviceImport = value;
    } else  if (data[i]["Indicator"] == "Commercial service exports (current US$)"){
      res[j].serviceExport = value;
    } else  if (data[i]["Indicator"] == "GDP (current US$)"){
      res[j].GDP = value;
    } else  if (data[i]["Indicator"] == "Total reserves (includes gold, current US$)"){
      res[j].reserve = value;
    } 
  }
  return res;
}

function dataName(v) {
  if (v == "International tourism, number of arrivals") return "arrival";
  else if (v == "International tourism, number of departures")
    return "departure";
  else if (v == "International tourism, expenditures (current US$)")
    return "expenditure";
  else if (v == "International tourism, receipts (current US$)")
    return "receipt";
  else if (v == "Commercial service imports (current US$)")
    return "serviceImport";
  else if (v == "Commercial service exports (current US$)")
    return "serviceExport";
  else if (v == "GDP (current US$)")
    return "GDP";
  else if (v == "Total reserves (includes gold, current US$)")
    return "reserve";
}

function loadControls(element, path) {
  var xhr = new XMLHttpRequest();
  xhr.open("GET", path, true);
  xhr.onreadystatechange = function() {
    if (this.readyState !== 4) return;
    if (this.status !== 200) return; // or whatever error handling you want
    document.getElementById(element).innerHTML = this.responseText;

    let item = new MDCSelect(document.querySelector("#xAxis_scat"));
    item.listen("MDCSelect:change", () => {
      selectVariable(0);
    });

    item.value = xValue;

    item = new MDCSelect(document.querySelector("#yAxis_scat"));
    item.listen("MDCSelect:change", () => {
      selectVariable(1);
    });

    item.value = yValue;
  };
  xhr.send();
}

export { Scatterplot };
