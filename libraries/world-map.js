import { Histogram } from "../libraries/histogram.js";
import { Scatterplot } from "../libraries/scatterplot.js";

let countryIdMapping;

const margin = {
  left: 50,
  right: 50,
  top: 50,
  bottom: 5
};

const width = window.innerWidth - margin.left - margin.right;
const height = window.innerHeight - margin.top - margin.bottom;

const MAP_HEIGHT_FULL = (window.innerHeight * 2) / 3;
const MAP_WIDTH_FULL = window.innerWidth / 2;
const MAP_HEIGHT_SMALL = MAP_HEIGHT_FULL * 0.4;

const HISTOGRAM_HEIGHT = window.innerHeight - MAP_HEIGHT_SMALL;

let HISTOGRAM_DISPLAYED = false;

const svg = d3
  .select("svg")
  .attr("width", width)
  .attr("height", height)
  .style("width", "100%")
  .style("height", "auto");

const rectG = svg.append("g");
const rectMap = rectG.append("rect");
rectMap
  .attr("height", MAP_HEIGHT_FULL)
  .attr("width", window.innerWidth)
  .attr("fill", "#fdfdfd");

const rectHist = rectG.append("rect");
rectHist
  .attr("height", HISTOGRAM_HEIGHT)
  .attr("width", window.innerWidth)
  .attr("y", MAP_HEIGHT_FULL)
  .attr("fill", "#fbfbfb");

const wraperG = svg.append("g");
const g = wraperG.append("g").attr("class", "all-G");
const legendG = g.append("g");
const mapG = g.append("g").attr("class", "map-G");
const slider = g.append("g").attr("class", "slider mdc-slider");

const histControls = svg
  .append("foreignObject")
  .attr("id", "histogram-controls")
  .attr("width", width);
const scatControls = svg
  .append("foreignObject")
  .attr("id", "scatterplot-controls")
  .attr("width", width)
  .style("visibility", "hidden");

let histogramG = svg.append("g");
let scatterplotG = svg.append("g");

const color = d3
  .scaleQuantize()
  .domain([20000000, 60000000])
  .range(d3.schemeBuPu[9]);

const projection = d3.geoEqualEarth();
let path = d3.geoPath(projection);

const tooltip = d3
  .select("body")
  .append("div")
  .style("visibility", "hidden")
  .attr("class", "tooltip mdc-card mdc-theme--on-surface");

tooltip.append("p");

const zoom = d3
  .zoom()
  .scaleExtent([0, 50])
  .translateExtent([[0, 0], [width + 90, height + 100]])
  .scaleExtent([0.8, 8])
  .on("zoom", d => zoomed(path));

let prevSliderVal,
  currentSliderVal = new Date(2015, 0, 0);

const WorldMap = {
  options: {
    width,
    height
  },
  drawMap: draw
};

let data = getData();
let us = getCountries();
let unfilteredData = [];

const scatterplot = new Scatterplot(
  MAP_HEIGHT_SMALL,
  unfilteredData,
  scatterplotG,
  scatControls
);

function draw() {
  d3.select("body").on("keypress", handleEnter);

  drawChart();
  initSlider();
}

async function getData() {
  return d3.csv("../data.csv").then(d => {
    unfilteredData = d;
    return _.filter(d, {
      Indicator: "International tourism, number of arrivals"
    });
  });
}

async function getCountries() {
  let countries = d3.json("/libraries/110m.json");
  return countries;
}

async function drawChart() {
  countryIdMapping = JSON.parse(
    await loadJSON("../libraries/reporterAreas.json")
  );
  const format = d3.format(".2s");

  const x = d3
    .scalePow()
    .domain(d3.extent(color.domain()))
    .rangeRound([600, 860]);

  wraperG.attr("transform", `translate(${(width - MAP_WIDTH_FULL) / 2},0)`);
  mapG.attr("transform", "translate(0, 20)");

  legendG
    .selectAll("rect")
    .data(color.range().map(d => color.invertExtent(d)))
    .enter()
    .append("rect")
    .attr("height", 8)
    .attr("x", d => x(d[0]))
    .attr("width", d => x(d[1]) - x(d[0]))
    .attr("fill", d => color(d[0]));

  legendG
    .append("text")
    .attr("class", "mdc-typography--caption	")
    .attr("x", x.range()[0])
    .attr("y", -6)
    .attr("class", "mdc-typography--caption	")
    .text("# of arrivals");

  legendG
    .call(
      d3
        .axisBottom(x)
        .tickSize(13)
        .tickFormat(format)
        .tickValues(
          color
            .range()
            .slice(1)
            .map(d => color.invertExtent(d)[0])
        )
    )
    .select(".domain")
    .remove();

  us = await us;
  data = await data;

  mapG
    .selectAll("path")
    .data(topojson.feature(us, us.objects.countries).features)
    .enter()
    .append("path")
    .attr("fill", d => {
      return getCountryColor(d.id);
    })
    .attr("d", path)
    .on("mouseover", handleMouseOver)
    .on("mouseout", handleMouseOut)
    .on("click", handleMouseClick);

  svg.call(zoom);

  return svg.node();
}

function getCountryColor(id) {
  let ctrName = getCountryNameById(id, countryIdMapping);
  let info = getNumberOfArrivals(ctrName);

  if (!info) {
    return "#ddd";
  }

  const noOfArrivals = parseInt(info)
  // console.log(noOfArrivals);
  // if (isNaN(noOfArrivals)) {
  //   console.log("naan" + info);
  // }
  return color(noOfArrivals);
}

function getNumberOfArrivals(countryName) {
  let info = _.filter(data, {
    "Country Name": countryName
  })[0];

  if (info === undefined) return null;

  return info[currentSliderVal.getFullYear().toString()];
}

function zoomed(path) {
  if (!HISTOGRAM_DISPLAYED) {
    g.attr("transform", d3.event.transform);
    rectG.attr("transform", d3.event.transform);
  }
}

function handleMouseOver(d, v) {
  let thisEl = d3.select(this);
  thisEl.attr("stroke", "black");
  let coords = d3.mouse(this);
  createTooltip(
    coords[0],
    coords[1],
    getCountryNameById(d.id, countryIdMapping)
  );

  if (HISTOGRAM_DISPLAYED) {
    enlargeMap();
  }
}

function enlargeMap() {
  HISTOGRAM_DISPLAYED = false;
  resizeMap(1, true);
  Histogram.destroy();
  scatterplot.destroy();
}

function handleMouseOut(d, v) {
  d3.select(this).attr("stroke", "none");
  tooltip.style("visibility", "hidden");
}

function handleMouseClick() {
  let item = d3.select(this);
  let isSelected = item.attr("class") === "selected";

  if (isSelected) {
    item.attr("class", "");
    return;
  }

  item.attr("class", "selected");
  // Histogram.draw(unfilteredData, selectedObjects);
}

function handleEnter(d, v) {
  if (d3.event.charCode === 13 && !HISTOGRAM_DISPLAYED) {
    let selectedObjects = d3.selectAll(".selected").data();
    let countryLabels = getCountryLabelsByObjects(selectedObjects);

    resizeMap(0.4);

    drawHistogram(countryLabels);
    scatterplot.draw(
      currentSliderVal.getFullYear(),
      unfilteredData,
      countryLabels
    );
    //console.log(currentSliderVal.getFullYear(), unfilteredData)
    return;
  }

  if (d3.event.charCode === 13 && HISTOGRAM_DISPLAYED) {
    enlargeMap();
  }
}

function drawHistogram(countryLabels) {
  Histogram.draw(
    countryLabels,
    currentSliderVal.getFullYear(),
    unfilteredData,
    histogramG,
    histControls,
    MAP_HEIGHT_SMALL
  );

  HISTOGRAM_DISPLAYED = true;
}

function resizeMap(scale, makeBigger) {
  if (!HISTOGRAM_DISPLAYED) {
    // g.attr("transform", "scale(1 1)");
    zoom.scaleTo(g, 1);
    // wraperG.attr("transform", `scale(${scale} ${scale})`);
    wraperG
      .transition()
      .duration(750)
      .attrTween("transform", d => {
        let i = d3.interpolate(1, scale);
        let moveRight = (width - MAP_WIDTH_FULL) / 2;

        if (makeBigger) i = d3.interpolate(0.4, 1);

        return d => {
          let str = `scale(${i(d)} ${i(d)}) `;
          if (makeBigger) str = str + `translate(${moveRight},0)`;

          return str;
        };
      })
      .attr("d", path);

    resizeRectange(rectMap, "height", makeBigger);
    resizeRectange(rectHist, "y", makeBigger);
  }
}

function resizeRectange(rect, attr, makeBigger) {
  if (makeBigger) {
    rect
      .transition()
      .duration(750)
      .attrTween(attr, d => {
        let i = d3.interpolate(MAP_HEIGHT_SMALL, MAP_HEIGHT_FULL);
        return d => {
          return i(d);
        };
      });
  } else {
    rect
      .transition()
      .duration(750)
      .attrTween(attr, d => {
        let i = d3.interpolate(MAP_HEIGHT_FULL, MAP_HEIGHT_SMALL);
        return d => {
          return i(d);
        };
      });
  }
}

function loadJSON(name, callback) {
  return new Promise(function(resolve, reject) {
    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open("GET", name, true); // Replace 'my_data' with the path to your file
    xobj.onreadystatechange = function() {
      if (xobj.readyState == 4 && xobj.status == "200") {
        if (callback === undefined) {
          resolve(xobj.responseText);
          return;
        }

        callback(xobj.responseText);
      }
    };
    xobj.send(null);
  });
}

function getCountryNameById(id, data) {
  let country = _.filter(data.results, {
    id: parseInt(id).toString()
  })[0];

  if (country === undefined) {
    return "";
  }

  return country.text;
}

function getCountryLabelsByObjects(objects) {
  let result = [];

  objects.forEach(e => result.push(getCountryNameById(e.id, countryIdMapping)));

  return result;
}

function updateColors() {
  console.log("UPDATING COLORS");
  console.log("YEAR: ", currentSliderVal.getFullYear());

  var selection = svg
    .selectAll("path")
    .data(topojson.feature(us, us.objects.countries).features);

  selection.exit().remove();

  selection.enter().append("path");

  selection
    .transition()
    .duration(750)
    .attr("fill", d => {
      return getCountryColor(d.id);
    })
    .attr("d", path);
}

function initSlider() {
  var dateFormat = d3.timeFormat("%Y");

  var x = d3
    .scaleTime()
    .domain([new Date(1999, 6, 1), new Date(2016, 6, 2)])
    .range([0, width / 2]);

  slider
    .append("line")
    .attr("class", "track")
    .attr("x1", x.range()[0])
    .attr("x2", x.range()[1])
    .select(function() {
      return this.parentNode.appendChild(this.cloneNode(true));
    })
    .attr("class", "track-inset")
    .select(function() {
      return this.parentNode.appendChild(this.cloneNode(true));
    })
    .attr("class", "track-overlay")
    .call(
      d3
        .drag()
        .on("start.interrupt", function(e) {
          console.log("SLIDER INTERUPT RECEIVED");
          slider.interrupt();
        })
        .on("end drag", d => {
          console.log("SLIDER END DRAG RECEIVED");
          var time = x.invert(d3.event.x);

          if (time.getFullYear() > 2016 || time.getFullYear() < 2000) {
            return;
          }

          prevSliderVal = currentSliderVal;
          let newDate = new Date(time.getFullYear(), 0, 0);
          currentSliderVal = newDate;
          console.log("CURRENT SLIDER VAL: " + currentSliderVal.getFullYear());
          moveHandle(newDate, handle, x);
          updateColors();
        })
    );

  slider
    .insert("g", ".track-overlay")
    .attr("class", "mdc-typography--caption	")
    .attr("transform", "translate(0," + 18 + ")")
    .selectAll("text")
    .data(x.ticks(10))
    .enter()
    .append("text")
    .attr("x", x)
    .attr("text-anchor", "middle")
    .text(d => dateFormat(d));

  var handle = slider
    .insert("circle", ".track-overlay")
    .attr("class", "handle")
    .attr("r", 9);

  slider.attr("transform", `translate(100, 530)`);

  moveHandle(currentSliderVal, handle, x);
}

function moveHandle(data, handle, x) {
  slider
    .transition()
    .duration(750)
    .tween("hue", function() {
      let prevVal = prevSliderVal === undefined ? new Date() : prevSliderVal;
      var i = d3.interpolate(x(prevVal), x(currentSliderVal));
      return function(t) {
        handle.attr("cx", i(t));
      };
    });
}

function createTooltip(x, y, text) {
  let trans = g.attr("transform");

  tooltip
    .style("visibility", "visible")
    .style("left", d3.event.pageX + 25 + "px")
    .style("top", d3.event.pageY + "px");

  const arrivals = getNumberOfArrivals(text);
  const getNumberOfArrivalstext = arrivals == null ? "Not known" : arrivals;

  tooltip.selectAll("*").remove();

  tooltip
    .append("p")
    .attr("class", "mdc-typography--body1	")
    .text(text);

  tooltip
    .append("p")
    .attr("class", "mdc-typography--body1	")
    .text(getNumberOfArrivalstext);
}

// function resize() {
//   let mapBox = wraperG.node().getBBox();
//   let moveLeftMap = (width - mapBox.width) / 2;
//
//   let sliderBox = slider.node().getBBox();
//   let moveDown = mapBox.height - 20;
//   let moveLeftSlider = (width - sliderBox.width) / 2;
//
//   // slider.attr("transform", `translate(${moveLeftSlider},${moveDown})`);
//   wraperG.attr("transform", `translate(${moveLeftMap},0)`);
// }
//
// svg.on("load", d => {
//   resize();
// });

export { WorldMap };
